var cityList = [{
        "CityId": 34,
        "CityName": "İstanbul",
        "DistrictList": [{
                "DistrictName": "Levent",
                "DistrictId": 341
            },
            {
                "DistrictName": "Avcılar",
                "DistrictId": 342
            },
            {
                "DistrictName": "Beylikdüzü",
                "DistrictId": 343
            }
        ]
    },
    {
        "CityId": 6,
        "CityName": "Ankara",
        "DistrictList": [{
                "DistrictName": "Çankaya",
                "DistrictId": 61
            },
            {
                "DistrictName": "Etimesgut",
                "DistrictId": 62
            },
            {
                "DistrictName": "Gölbaşı",
                "DistrictId": 63
            }
        ]
    }
];

var productList = [{
        "ProductId": 1,
        "ProductTitle": "En Küçük",
        "ProductPrice": 30,
        "ProductTypes": [
            "Avize",
            "Komodin",
            "Sandalye",
            "Sehpa",
            "Stor Perde",
            "Tablo Asma"
        ]
    },
    {
        "ProductId": 2,
        "ProductTitle": "Küçük",
        "ProductPrice": 60,
        "ProductTypes": [
            "Ayakkabılık",
            "Berjer",
            "Banyo Dolabı",
            "Duvar Rafı",
            "Kitaplık",
            "Şifonyer"
        ]
    },
    {
        "ProductId": 3,
        "ProductTitle": "Orta",
        "ProductPrice": 100,
        "ProductTypes": [
            "Büfe/Vitrin",
            "Konsol",
            "Masa",
            "Portmanto",
            "Tek Kapılı Gardrop",
            "Televizyon Sehpası"
        ]
    },
    {
        "ProductId": 4,
        "ProductTitle": "Büyük",
        "ProductPrice": 130,
        "ProductTypes": [
            "Çok Kapılı Gardrop",
            "Duvar TV Ünitesi",
            "Karyola",
            "Koltuk Takımı",
            "Ranza",
            "TV Ünitesi"
        ]
    }
];

var hours = [{
        'id': 1,
        'name': "08:00"
    },
    {
        'id': 2,
        'name': "08:30"
    },
    {
        'id': 3,
        'name': "09:00"
    },
    {
        'id': 4,
        'name': "09:30"
    },
    {
        'id': 5,
        'name': "10:00"
    },
    {
        'id': 6,
        'name': "10:30"
    },
    {
        'id': 7,
        'name': "11:00"
    },
    {
        'id': 8,
        'name': "11:30"
    },
    {
        'id': 9,
        'name': "12:00"
    },
    {
        'id': 10,
        'name': "12:30"
    },
    {
        'id': 11,
        'name': "13:00"
    },
    {
        'id': 12,
        'name': "13:30"
    },
    {
        'id': 13,
        'name': "14:00"
    },
    {
        'id': 14,
        'name': "14:30"
    },
    {
        'id': 15,
        'name': "15:00"
    },
    {
        'id': 16,
        'name': "15:30"
    }
]