var summary = {
    data: function () {
        return {
           
        }
    },
    props: ['values', 'messages', 'products', 'totalprice'],
    methods: {
        checkForm(e) {
            this.$parent.checkForm(e);
        }
    },
    template: '#summary-card',
};

var productComponent = {
    data: function () {
        return {

        }
    },
    props: ['item', 'district', 'datetype', 'cleantime', 'pickerval'],
    methods: {
        productCode(id) {
            return this.$parent.productCode(id);
        },
        addtoBasket(item) {
            this.$parent.addtoBasket(item);
        },
        removeFromBasket(item) {
            this.$parent.removeFromBasket(item);
        }
    },
    template: `
    <div class="col-12 mb-4">
    <div class="media shadow-sm rounded p-4">
        <div class="align-self-center mr-3 badge badge-pill badge-danger">
            {{productCode(item.ProductId)}}
        </div>
        <div class="media-body">
            <div class="row">
                <div class="col-9">
                    <h5 class="mt-0">{{item.ProductTitle}} {{item.ProductPrice}} TL/ADET</h5>
                    <p class="mb-0">
                        <span v-for="type in item.ProductTypes">
                            <span class="pr-2">{{type}}</span>
                        </span>
                    </p>

                </div>
                <div class="col-3 text-right align-self-center">

                    <div class="btn-group" data-toggle="buttons">
                        <button type="button" v-on:click="removeFromBasket(item)"
                            class="btn btn-light"
                            :disabled="!district || !datetype || (datetype == 3 && !pickerval) || !cleantime">
                            -
                        </button>
                        <button type="button" class="btn btn-light" disabled>
                            {{item.quantity}}
                        </button>
                        <button type="button" v-on:click="addtoBasket(item)"
                            class="btn btn-light"
                            :disabled="!district || !datetype || (datetype == 3 && !pickerval) || !cleantime">
                            +
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
`
};

var app = new Vue({
    el: '#app',
    data: {
        products: productList,
        cities: cityList,
        districts: [],
        city: '34',
        district: '',
        dateType: '',
        pickerVal: '',
        cleanTime: '',
        hours: hours,
        note: '',
        totalPrice: 0,
        errors: [],
        messages: [],
        values: null
    },
    components: {
        'product': productComponent,
        'summary-card': summary
    },
    watch: {
        city: function (val) {
            var selectedCity = this.cities.filter(city => city.CityId == val);
            this.districts = selectedCity[0].DistrictList;
            this.resetAllValues('city');
            this.getMessages();
        },
        district: function (val) {
            this.resetAllValues('district');
            this.getMessages();
        },
        pickerVal: function (val) {
            this.cleanTime = "";
            this.getMessages();
        },
        dateType: function (val) {
            this.getMessages();
        },
        cleanTime: function (val) {
            this.getMessages();
        }
    },
    methods: {
        productCode: function (ProductId) {
            var code = "";

            switch (ProductId) {
                case 1:
                    code = "XS";
                    break;
                case 2:
                    code = "S";
                    break;
                case 3:
                    code = "M";
                    break;
                case 4:
                    code = "L";
                    break;
                default:
                    code = "";
            }

            return code;

        },
        addtoBasket: function (item) {
            var quantity = item.quantity;
            var newVal = quantity + 1;
            this.$set(item, 'quantity', newVal);
            this.totalPrice = this.getTotalPrice();
        },
        removeFromBasket: function (item) {
            var quantity = item.quantity;
            var newVal = (quantity - 1 >= 0) ? quantity - 1 : 0;
            this.$set(item, 'quantity', newVal);
            this.totalPrice = this.getTotalPrice();
        },
        resetAllValues: function (type) {
            if (type == 'city') {
                this.district = "";
            }
            this.cleanTime = "";
            this.note = "";
            this.pickerVal = "";
            this.dateType = "";
            this.total = 0;

            this.products.map(item => {
                this.$set(item, 'quantity', 0);
            });
        },
        getTotalPrice: function () {
            var total = 0;

            this.products.forEach(product => {
                total += parseInt(product.quantity) * product.ProductPrice;
            });

            return total;
        },
        getMessages: function () {

            var messages = [];

            if (!this.city) {
                messages.push('Şehir seçilmedi');
            }

            if (!this.district) {
                messages.push('İlçe seçilmedi');
            }

            if (!this.dateType) {
                messages.push('Tarih seçilmedi');
            }

            if (this.dateType == 3 && !this.pickerVal) {
                messages.push('Tarih seçilmedi');
            }

            if (!this.cleanTime) {
                messages.push('Saat seçilmedi');
            }


            this.getValues();

            this.messages = messages;
            return messages;

        },
        getValues: function () {

            this.values = {};

            var values = {};

            if (this.city && this.district && this.cleanTime && this.dateType) {
                var selectedCity = this.cities.filter(city => city.CityId == this.city);
                var selectedDistrict = this.districts.filter(item => item.DistrictId == this.district);
                var selectedTime = this.hours.filter(item => item.id == this.cleanTime);
                var today = new Date();
                var tomorrow = new Date();
                tomorrow.setDate(today.getDate() + 1);
                var datevalues = [
                    today,
                    tomorrow
                ];

                values['CityName'] = selectedCity[0].CityName;
                values['DistrictName'] = selectedDistrict[0].DistrictName;
                values['SelectedDate'] = this.dateType == 3 ? this.formatDate(new Date(this.pickerVal)) : this.formatDate(datevalues[this.dateType - 1]);
                values['SelectedTime'] = selectedTime[0].name;
            }

            this.values = values;

        },
        checkForm: function (e) {
            var msgs = this.getMessages();
            this.errors = msgs.splice(0);

            if (!this.totalPrice) {
                this.errors.push('Devam edebilmek için montajı yapılacak ürün veya ürünleri seçmen gerek.');
            }

            if (this.errors.length) {
                alert('Sanırım biraz daha bilgi vermen gerekiyor.');
                e.preventDefault();
            } else {
                alert('Tebrikler. Evinizdeki mutluluğa sadece birkaç adım kaldı.')
                e.preventDefault();
            }


        },
        formatDate(date) {
            var monthNames = [
                "Ocak", "Şubat", "Mart",
                "Nisan", "Mayıs", "Haziran", "Temmuz",
                "Ağustos", "Eylül", "Ekim",
                "Kasım", "Aralık"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
    },
    created: function () {

        this.getMessages();

        var selectedCity = this.cities.filter(city => city.CityId == this.city);
        this.districts = selectedCity[0].DistrictList;

        this.products.map(item => {
            this.$set(item, 'quantity', 0);
        });


    },
    mounted() {
        // var self = this;

        // fetch("data/cities.json")
        //     .then(r => r.json())
        //     .then(json => {
        //         self.cities = json;
        //         var selectedCity = this.cities.filter(city => city.CityId == this.city);
        //         this.districts = selectedCity[0].DistrictList;
        //     });

        // fetch("data/products.json")
        //     .then(r => r.json())
        //     .then(json => {
        //         self.products = json;
        //         self.products.map(item => {
        //             self.$set(item, 'quantity', 0);
        //         });

        //     });

    }
})